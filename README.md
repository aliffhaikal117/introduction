__This page contain:__

### Introduction

### Project

### Conclusion

# Introduction

### Biography

<img src="https://gitlab.com/aliffhaikal117/introduction/-/raw/main/Files/197273.png" width=150 align=middle>

+ Name = Muhamad Aliff Haikal bin Biamin
+ Age = 22 years old
+ From = Kuala Nerus, Terengganu
+ Course = Bachelor of Aerospace Engineering


### Strength

1. Good in archery

2. Not bad coding

3. Cost-efficient

### Weakness

1. Eyesight

2. Time-management

3. Communication

# Project

### Python Programming

+ Calculation of [Compound Annual Growth Rate(CAGR)](https://www.investopedia.com/terms/c/cagr.asp)

The code is

``` js
Year=float(input('Years Accumulated: '))

Ini=float(input('Initial Investment Value: '))

Cur=float(input('Current Investment Value: '))

CAGR=((Cur/Ini)**(1/Year))-1

print(CAGR*100,'%')

```
*Example*

Abu, Ali and Abas are investing in different cryptocurrency with initial investment of RM100. After 10 years, they come back to check their investment value. The current value of their investment is shown below:
|Name|Current Value|
| ---- |-----|
|Abu|200|
|Ali|1000|
|Abas|150|

After calculation, they are able to see that Ali's investment perform the best among them. This may suggest that Ali has the best investment due to impressive CAGR which grow 25% annually.
|Name|Initial Value|Current Value|CAGR(%)|
| ---- |-----|----|----|
|Abu|100|200|7|
|Ali|100|1000|25|
|Abas|100|150|4|

# Conclusion

In conclusion, markdown language is not bad. It is quiet straight forward. I will keep learning it to be able to use it from any possible way.












